import React from 'react';
import RemoteDashboard from './remote-dashboard/RemoteDashboard';

function App() {
  return (
    <div className="App">
      <RemoteDashboard />
    </div>
  );
}

export default App;
